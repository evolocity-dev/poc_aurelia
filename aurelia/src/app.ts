import {Router, RouterConfiguration} from 'aurelia-router';


export class App {
  title = 'Tour of Heroes - Aurelia';

  private router: Router;

  public configureRouter(config: RouterConfiguration, router: Router) {
    config.title = 'Aurelia Tour of Heroes';
    config.map([
      {route: 'heroes', name: 'heroes', moduleId: 'heroes/heroes', title: 'Heroes', nav: true},
      {route: 'hero-detail/:id', name: 'hero-detail', moduleId: 'heroes/hero-detail', title: 'Hero Detail'},
      {route: ['', 'dashboard'], name: 'dashboard', moduleId: 'dashboard/dashboard', title: 'Dashboard', nav: true}
    ]);

    this.router = router;
  }
}
