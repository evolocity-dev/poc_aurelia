import {HttpClient} from 'aurelia-http-client';

import {Hero} from '../heroes/hero';

export class HeroService {
    client: HttpClient;

    constructor() {
      this.client = new HttpClient();
    }

    getHeroes(): Promise<Array<Hero>> {
      return this.client
                // .get('/heroes')
                .get('/lots/of/heroes')
                .then(data => {
                  return JSON.parse(data.response);
                });
    }

    getHeroById(id: string): Promise<Hero> {
      return this.client
                .get('/heroes/' + id)
                .then(data => {
                  return JSON.parse(data.response);
                });
    }
}
