import {inject} from 'aurelia-framework';
import {Router} from 'aurelia-router';

import { Hero } from '../heroes/hero'
import { HeroService } from '../services/hero.service'

@inject(HeroService, Router)
export class DashboardComponent {
  router: Router;
  heroService: HeroService;
  heroes: Hero[];

  constructor(heroService, router) {
    this.heroService = heroService;
    this.router = router;
  }

  activate(params) {
    return this.heroService
            .getHeroes()
            .then(heroes => {
              this.heroes = heroes.slice(1,5)
            });
  }

  gotoDetail(hero: Hero): void {
      this.router.navigateToRoute('hero-detail', {id: hero.id})
  }
}
