import {inject} from 'aurelia-framework';
import {Router} from 'aurelia-router';

import {Hero} from './hero'
import {HeroService} from '../services/hero.service'

@inject(HeroService, Router)
export class App {
  heroService: HeroService;
  heroes: Hero[];
  selectedHero: Hero;
  router: Router;

  title = 'Tour of Heroes - Aurelia';

  constructor(heroService, router) {
    this.heroService = heroService;
    this.router = router;
  }

  activate(params) {
    return this.heroService
            .getHeroes()
            .then(heroes => this.heroes = heroes);
  }

  gotoDetail(): void {
      this.router.navigateToRoute('hero-detail', {id: this.selectedHero.id})
  }

  onSelect(hero: Hero): void {
    this.selectedHero = hero;
  }
}
