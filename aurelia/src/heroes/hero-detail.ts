import {inject} from 'aurelia-framework';

import {Hero} from './hero'
import {HeroService} from '../services/hero.service'

@inject(HeroService)
export class HeroDetails{
  heroService: HeroService
  selectedHero: Hero

  constructor(heroService) {
    this.heroService = heroService;
  }

  activate(params: any) {
    return this.heroService
            .getHeroById(params.id)
            .then((hero: Hero) => this.selectedHero = hero);
  }

  goBack(): void {
    window.history.back();
  }
}
