define('app',["require", "exports"], function (require, exports) {
    "use strict";
    var App = (function () {
        function App() {
            this.title = 'Tour of Heroes - Aurelia';
        }
        App.prototype.configureRouter = function (config, router) {
            config.title = 'Aurelia Tour of Heroes';
            config.map([
                { route: 'heroes', name: 'heroes', moduleId: 'heroes/heroes', title: 'Heroes', nav: true },
                { route: 'hero-detail/:id', name: 'hero-detail', moduleId: 'heroes/hero-detail', title: 'Hero Detail' },
                { route: ['', 'dashboard'], name: 'dashboard', moduleId: 'dashboard/dashboard', title: 'Dashboard', nav: true }
            ]);
            this.router = router;
        };
        return App;
    }());
    exports.App = App;
});

define('environment',["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.default = {
        debug: true,
        testing: true
    };
});

define('main',["require", "exports", './environment'], function (require, exports, environment_1) {
    "use strict";
    Promise.config({
        warnings: {
            wForgottenReturn: false
        }
    });
    function configure(aurelia) {
        aurelia.use
            .standardConfiguration()
            .feature('resources');
        if (environment_1.default.debug) {
            aurelia.use.developmentLogging();
        }
        if (environment_1.default.testing) {
            aurelia.use.plugin('aurelia-testing');
        }
        aurelia.start().then(function () { return aurelia.setRoot(); });
    }
    exports.configure = configure;
});

define('heroes/hero',["require", "exports"], function (require, exports) {
    "use strict";
    var Hero = (function () {
        function Hero() {
        }
        return Hero;
    }());
    exports.Hero = Hero;
});

define('services/hero.service',["require", "exports", 'aurelia-http-client'], function (require, exports, aurelia_http_client_1) {
    "use strict";
    var HeroService = (function () {
        function HeroService() {
            this.client = new aurelia_http_client_1.HttpClient();
        }
        HeroService.prototype.getHeroes = function () {
            return this.client
                .get('/lots/of/heroes')
                .then(function (data) {
                return JSON.parse(data.response);
            });
        };
        HeroService.prototype.getHeroById = function (id) {
            return this.client
                .get('/heroes/' + id)
                .then(function (data) {
                return JSON.parse(data.response);
            });
        };
        return HeroService;
    }());
    exports.HeroService = HeroService;
});

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
define('dashboard/dashboard',["require", "exports", 'aurelia-framework', 'aurelia-router', '../services/hero.service'], function (require, exports, aurelia_framework_1, aurelia_router_1, hero_service_1) {
    "use strict";
    var DashboardComponent = (function () {
        function DashboardComponent(heroService, router) {
            this.heroService = heroService;
            this.router = router;
        }
        DashboardComponent.prototype.activate = function (params) {
            var _this = this;
            return this.heroService
                .getHeroes()
                .then(function (heroes) {
                _this.heroes = heroes.slice(1, 5);
            });
        };
        DashboardComponent.prototype.gotoDetail = function (hero) {
            this.router.navigateToRoute('hero-detail', { id: hero.id });
        };
        DashboardComponent = __decorate([
            aurelia_framework_1.inject(hero_service_1.HeroService, aurelia_router_1.Router), 
            __metadata('design:paramtypes', [Object, Object])
        ], DashboardComponent);
        return DashboardComponent;
    }());
    exports.DashboardComponent = DashboardComponent;
});

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
define('heroes/hero-detail',["require", "exports", 'aurelia-framework', '../services/hero.service'], function (require, exports, aurelia_framework_1, hero_service_1) {
    "use strict";
    var HeroDetails = (function () {
        function HeroDetails(heroService) {
            this.heroService = heroService;
        }
        HeroDetails.prototype.activate = function (params) {
            var _this = this;
            return this.heroService
                .getHeroById(params.id)
                .then(function (hero) { return _this.selectedHero = hero; });
        };
        HeroDetails.prototype.goBack = function () {
            window.history.back();
        };
        HeroDetails = __decorate([
            aurelia_framework_1.inject(hero_service_1.HeroService), 
            __metadata('design:paramtypes', [Object])
        ], HeroDetails);
        return HeroDetails;
    }());
    exports.HeroDetails = HeroDetails;
});

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
define('heroes/heroes',["require", "exports", 'aurelia-framework', 'aurelia-router', '../services/hero.service'], function (require, exports, aurelia_framework_1, aurelia_router_1, hero_service_1) {
    "use strict";
    var App = (function () {
        function App(heroService, router) {
            this.title = 'Tour of Heroes - Aurelia';
            this.heroService = heroService;
            this.router = router;
        }
        App.prototype.activate = function (params) {
            var _this = this;
            return this.heroService
                .getHeroes()
                .then(function (heroes) { return _this.heroes = heroes; });
        };
        App.prototype.gotoDetail = function () {
            this.router.navigateToRoute('hero-detail', { id: this.selectedHero.id });
        };
        App.prototype.onSelect = function (hero) {
            this.selectedHero = hero;
        };
        App = __decorate([
            aurelia_framework_1.inject(hero_service_1.HeroService, aurelia_router_1.Router), 
            __metadata('design:paramtypes', [Object, Object])
        ], App);
        return App;
    }());
    exports.App = App;
});

define('resources/index',["require", "exports"], function (require, exports) {
    "use strict";
    function configure(config) {
    }
    exports.configure = configure;
});

define('text!app.html', ['module'], function(module) { module.exports = "<template>\n  <require from=\"./app.css\"></require>\n\n  <h1>${title}</h1>\n\n  <nav>\n     <a route-href=\"route: dashboard\">Dashboard</a>\n     <a route-href=\"route: heroes\">Heroes</a>\n  </nav>\n\n  <router-view></router-view>\n</template>\n"; });
define('text!app.css', ['module'], function(module) { module.exports = "h1 {\n  font-size: 1.2em;\n  color: #999;\n  margin-bottom: 0;\n}\nh2 {\n  font-size: 2em;\n  margin-top: 0;\n  padding-top: 0;\n}\nnav a {\n  padding: 5px 10px;\n  text-decoration: none;\n  margin-top: 10px;\n  display: inline-block;\n  background-color: #eee;\n  border-radius: 4px;\n}\nnav a:visited, a:link {\n  color: #607D8B;\n}\nnav a:hover {\n  color: #039be5;\n  background-color: #CFD8DC;\n}\nnav a.active {\n  color: #039be5;\n}\n"; });
define('text!dashboard/dashboard.html', ['module'], function(module) { module.exports = "<template>\n  <require from=\"./dashboard.css\"></require>\n\n  <h3>Top Heroes</h3>\n  \n  <div class=\"grid grid-pad\">\n    <div repeat.for=\"hero of heroes\" click.delegate=\"gotoDetail(hero)\" class=\"col-1-4\">\n      <div class=\"module hero\">\n        <h4>${hero.name}</h4>\n      </div>\n    </div>\n  </div>\n</template>\n"; });
define('text!dashboard/dashboard.css', ['module'], function(module) { module.exports = "[class*='col-'] {\n  float: left;\n}\n*, *:after, *:before {\n    -webkit-box-sizing: border-box;\n    -moz-box-sizing: border-box;\n    box-sizing: border-box;\n}\nh3 {\n  text-align: center; margin-bottom: 0;\n}\n[class*='col-'] {\n  padding-right: 20px;\n  padding-bottom: 20px;\n}\n[class*='col-']:last-of-type {\n  padding-right: 0;\n}\n.grid {\n  margin: 0;\n}\n.col-1-4 {\n  width: 25%;\n}\n.module {\n    padding: 20px;\n    text-align: center;\n    color: #eee;\n    max-height: 120px;\n    min-width: 120px;\n    background-color: #607D8B;\n    border-radius: 2px;\n}\nh4 {\n  position: relative;\n}\n.module:hover {\n  background-color: #EEE;\n  cursor: pointer;\n  color: #607d8b;\n}\n.grid-pad {\n  padding: 10px 0;\n}\n.grid-pad > [class*='col-']:last-of-type {\n  padding-right: 20px;\n}\n@media (max-width: 600px) {\n    .module {\n      font-size: 10px;\n      max-height: 75px; }\n}\n@media (max-width: 1024px) {\n    .grid {\n      margin: 0;\n    }\n    .module {\n      min-width: 60px;\n    }\n}\n"; });
define('text!heroes/hero-detail.html', ['module'], function(module) { module.exports = "<template>\n  <require from=\"./hero-detail.css\"></require>\n\n  <h2>${selectedHero.name} details!</h2>\n\n  <div>\n    <label>id: </label>${selectedHero.id}\n  </div>\n  <div>\n    <label>name: </label>\n    <input type=\"text\" value.two-way=\"selectedHero.name\" placeholder=\"name\"/>\n  </div>\n  <button click.delegate=\"goBack()\">Back</button>\n</template>\n"; });
define('text!heroes/hero-detail.css', ['module'], function(module) { module.exports = "label {\n  display: inline-block;\n  width: 3em;\n  margin: .5em 0;\n  color: #607D8B;\n  font-weight: bold;\n}\ninput {\n  height: 2em;\n  font-size: 1em;\n  padding-left: .4em;\n}\nbutton {\n  margin-top: 20px;\n  font-family: Arial;\n  background-color: #eee;\n  border: none;\n  padding: 5px 10px;\n  border-radius: 4px;\n  cursor: pointer; cursor: hand;\n}\nbutton:hover {\n  background-color: #cfd8dc;\n}\nbutton:disabled {\n  background-color: #eee;\n  color: #ccc;\n  cursor: auto;\n}\n"; });
define('text!heroes/heroes.html', ['module'], function(module) { module.exports = "<template>\n  <require from=\"./heroes.css\"></require>\n\n  <h2>My Heroes</h2>\n\n  <ul class=\"heroes\">\n    <li repeat.for=\"hero of heroes\"\n        click.delegate=\"onSelect(hero)\"\n        class=\"${ hero === selectedHero ? 'selected' : '' }\">\n      <span class=\"badge\">${hero.id}</span> ${hero.name}\n    </li>\n  </ul>\n  <div if.bind=\"selectedHero\">\n    <h2>\n      ${selectedHero.name.toUpperCase()} is my hero\n    </h2>\n    <button click.delegate=\"gotoDetail()\">View Details</button>\n  </div>\n</template>\n"; });
define('text!heroes/heroes.css', ['module'], function(module) { module.exports = ".selected {\n  background-color: #CFD8DC !important;\n  color: white;\n}\n.heroes {\n  margin: 0 0 2em 0;\n  list-style-type: none;\n  padding: 0;\n  width: 15em;\n}\n.heroes li {\n  cursor: pointer;\n  position: relative;\n  left: 0;\n  background-color: #EEE;\n  margin: .5em;\n  padding: .3em 0;\n  height: 1.6em;\n  border-radius: 4px;\n}\n.heroes li.selected:hover {\n  background-color: #BBD8DC !important;\n  color: white;\n}\n.heroes li:hover {\n  color: #607D8B;\n  background-color: #DDD;\n  left: .1em;\n}\n.heroes .text {\n  position: relative;\n  top: -3px;\n}\n.heroes .badge {\n  display: inline-block;\n  font-size: small;\n  color: white;\n  padding: 0.8em 0.7em 0 0.7em;\n  background-color: #607D8B;\n  line-height: 1em;\n  position: relative;\n  left: -1px;\n  top: -4px;\n  height: 1.8em;\n  margin-right: .8em;\n  border-radius: 4px 0 0 4px;\n}\n"; });
//# sourceMappingURL=app-bundle.js.map