**UI Framework / library - Proof of concept**

Implementing the [Tour of Heroes Demo](http://plnkr.co/edit/?p=preview) in [AureliaJS](http://aurelia.io/)

To start the server (runs on port 8080):
> gradle springBoot

The server has 2 endpoints:

1. /heroes - list of 11 heroes
2. /lots/of/heroes list of 10 000 heroes

The server serves the static files located under: */src/main/resources/static/*